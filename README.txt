Webform Placeholder Component
------------------------
by Sergei Tarassov

Description
-----------
This is a small and efficient additional component for webforms which add
component for webforms. Component is a text with tokens which provide
access to any casual and submission data via tokens to display it in as
markup component. No setup needed, just add placeholder component to webform.
Initially the module rose from the task http://drupal.org/node/1672890

Known problems
--------------
Webform uses a custom invoke functions (webform_component_invoke) which does not
prefix the module name before the function call. Functions in
components/placeholders.inc are therefore not namespaced, when/if the webform
module changes this, I will add the correct namespacing.

Installation
------------
 * Copy the module's directory to your modules directory and activate the
 module.
