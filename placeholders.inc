<?php

/**
 * @file
 * Webform module placeholders component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_placeholders() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'private' => TRUE,
      'hidden_type' => TRUE,
      'title_display' => 1,
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_placeholders($component) {
  $form = array();
  $form['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Text'),
    '#default_value' => $component['value'],
    '#description' => theme('webform_token_help'),
    '#cols' => 60,
    '#rows' => 5,
    '#weight' => 0,
  );

  return $form;
}


/**
 * Implements _webform_render_component().
 */
function _webform_render_placeholders($component, $value = NULL, $filter = TRUE) {

  $element = array(
    '#type' => 'markup',
    '#title' => '',
    '#value' => '',
  );

  return $element;

}

/**
 * Implements _webform_display_component().
 */
function _webform_display_placeholders($component, $value, $format = 'html') {

  $element = array(
    '#title' => $component['name'],
    '#markup' => isset($value[0]) ? $value[0] : NULL,
    '#weight' => $component['weight'],
    '#format' => $format,
    '#theme_wrappers' => $format == 'text' ? array('webform_element_text') : array('webform_element'),
    '#translatable' => array('title'),
  );

  return $element;
}

/**
 * Theme function.
 *
 * @param {array} $variables
 *   theme variables
 *
 * @return string
 *   markup
 */
function theme_webform_display_placeholders($variables) {
  $element = $variables['element'];

  return $element['#format'] == 'html' ? check_plain($element['#markup']) : $element['#markup'];
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_placeholders($component, $sids = array()) {

  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('no', 'data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid']);

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $submissions = 0;
  $wordcount = 0;

  $result = $query->execute();
  foreach ($result as $data) {
    if (drupal_strlen(trim($data['data'])) > 0) {
      $nonblanks++;
      $wordcount += str_word_count(trim($data['data']));
    }
    $submissions++;
  }

  $rows[0] = array(t('Empty'), ($submissions - $nonblanks));
  $rows[1] = array(t('Non-empty'), $nonblanks);
  $rows[2] = array(
    t('Average submission length in words (ex blanks)'),
    ($nonblanks != 0 ? number_format($wordcount / $nonblanks, 2) : '0'),
  );
  return $rows;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_table_placeholders($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_headers_placeholders($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_placeholders($component, $export_options, $value) {
  return isset($value[0]) ? $value[0] : '';
}
